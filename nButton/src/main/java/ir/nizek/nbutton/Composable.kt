package ir.nizek.nbutton

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView


@Composable
fun NizekButton(
    onClick: () -> Unit,
    title: String?,
    subTitle: String? = null,
    icon: Int? = null
) {
    val button = CustomButton(LocalContext.current)
    button.titleText = title
    button.subTitleText = subTitle
    button.icon = icon
    button.setOnClickListener { onClick.invoke() }
    button.createView()

    AndroidView({ button })
}