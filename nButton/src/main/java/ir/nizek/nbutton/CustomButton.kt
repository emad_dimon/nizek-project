package ir.nizek.nbutton

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat

class CustomButton(context: Context?) :
    LinearLayout(context) {
    constructor(context: Context, attrs: AttributeSet) : this(context) {
        initView(attrs)
    }
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : this(context) {
        initView(attrs)
    }

    var titleText: String? = null
    var subTitleText: String? = null
    var icon: Int? = null

    fun createView() {
        initView()
    }

    private fun initView(attrs: AttributeSet? = null) {
        isClickable = true
        isFocusable = true

        if (attrs != null) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.CustomButton)
        titleText = ta.getString(R.styleable.CustomButton_title)
        subTitleText = ta.getString(R.styleable.CustomButton_subtitle)
    }


        setBackgroundResource(R.drawable.background)
        orientation = VERTICAL
        gravity = Gravity.CENTER

        val textLayoutParam = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )

        val title = TextView(context)
        title.id = View.generateViewId()
        title.text = titleText
        title.setTextColor(ContextCompat.getColor(context, R.color.title_color))
        icon?.let {
            val resource = it
            title.setCompoundDrawablesWithIntrinsicBounds(resource, 0, 0, 0,)
        }
        addView(title, textLayoutParam)

        if(subTitleText == null)
            return

        val subtitle = TextView(context)
        subtitle.id = View.generateViewId()
        subtitle.text = subTitleText
        subtitle.setTextColor(ContextCompat.getColor(context, R.color.subtitle_color))
        addView(subtitle, textLayoutParam)
    }
}