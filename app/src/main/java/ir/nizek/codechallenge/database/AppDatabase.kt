package ir.nizek.codechallenge.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ir.nizek.codechallenge.database.dao.UserDao
import ir.nizek.codechallenge.database.entity.UserEntity

@Database(entities = [UserEntity::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun getUserDao(): UserDao
}