package ir.nizek.codechallenge.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ir.nizek.codechallenge.database.entity.UserEntity
import ir.nizek.codechallenge.utiles.userDatabase

@Dao
interface UserDao {
    @Insert
    suspend fun insert(vararg users: UserEntity): List<Long>

    @Query("SELECT * FROM $userDatabase WHERE username = :username")
    suspend fun getUser(username: String): List<UserEntity>
}