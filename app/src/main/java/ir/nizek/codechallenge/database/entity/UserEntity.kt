package ir.nizek.codechallenge.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ir.nizek.codechallenge.utiles.userDatabase

@Entity(tableName = userDatabase, indices = [Index(value = ["username"], unique = false)])
data class UserEntity(
    @ColumnInfo(name = "full_name") val name: String?,
    @ColumnInfo(name = "username") val username: String,
    @ColumnInfo(name = "password") val password: String,
    @ColumnInfo(name = "created_at") val createdAt: Long?,
    @ColumnInfo(name = "updated_at") val updatedAt: Long?,
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
}