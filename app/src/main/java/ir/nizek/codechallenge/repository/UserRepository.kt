package ir.nizek.codechallenge.repository

import ir.nizek.codechallenge.database.dao.UserDao
import ir.nizek.codechallenge.domain.user.User
import ir.nizek.codechallenge.domain.user.UserMapper
import java.lang.Exception
import javax.inject.Inject

class UserRepository
@Inject
constructor(val dao: UserDao) {
    suspend fun insert(user: User): Long {
        val userEntity = UserMapper().domainModelToEntity(user)
        return dao.insert(userEntity)[0]
    }

    suspend fun getUser(username: String): User {
        val userEntity = dao.getUser(username)
        return if (userEntity.isNotEmpty())
            UserMapper().entityToDomainModel(userEntity[0])
        else
            throw Exception("User Not Found")
    }
}