package ir.nizek.codechallenge.utiles.state

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*

class LimitationOnUsage(private val builder: Builder): ComponentState(builder.lifecycleOwner) {
    private var counter: Long = builder.allowInForeground ?: -1
    private var job: Job? = null
    private var handler = CoroutineExceptionHandler {_, throwable ->
        throwable.message?.let { message ->
            builder.errorHandler?.invoke(message)
        }
    }

    fun start() {
        job = builder
            .lifecycleOwner
            .lifecycleScope.launch(Dispatchers.Default + handler) {
                    while (counter > 0 && isActive) {
                        delay(1000)
                        --counter
                        if (counter == 0L) {
                            builder.callback?.invoke()
                            release()
                        }
                    }
        }
    }

    override fun componentInForeground() {
        counter = builder.allowInForeground ?: -1
    }

    override fun componentInBackground() {
        counter = builder.allowInBackground ?: -1
    }

    override fun release() {
        job?.cancel()
    }

    class Builder(val lifecycleOwner: LifecycleOwner) {
        var allowInForeground: Long? = null
        var allowInBackground: Long? = null
        var callback: (() -> Unit)? = null
        var dispatcher = Dispatchers.Default
        var errorHandler: ((String) -> Unit)? = null

        fun foregroundLimitation(timeInMillis: Long) = apply { allowInForeground = timeInMillis }
        fun backgroundLimitation(timeInMillis: Long) = apply { allowInBackground = timeInMillis }
        fun setCallBack(callback: (() -> Unit)?) = apply { this.callback = callback }
        fun errorHandler(callback: ((String) -> Unit)?) = apply { this.errorHandler = callback}
        fun dispatcher(dispatcher: CoroutineDispatcher) = apply { this.dispatcher = dispatcher }

        fun build() {
            LimitationOnUsage(this)
                .start()
        }
    }
}