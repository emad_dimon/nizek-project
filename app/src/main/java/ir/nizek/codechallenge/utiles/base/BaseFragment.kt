package ir.nizek.codechallenge.utiles.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BaseFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ComposeView(requireContext()).apply {
            initView(this)
            observer()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    abstract fun observer()
    abstract fun initView(compose: ComposeView)
}