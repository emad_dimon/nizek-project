package ir.nizek.codechallenge.utiles


import at.favre.lib.crypto.bcrypt.BCrypt
import kotlinx.coroutines.Job
import java.util.*

const val dbName = "jarchi"
const val userDatabase = "raaste_user"
const val userSharedPrefKey = "USER"
const val DELAY = 1000L
const val TIME_IN_BACKGROUND = 30L
const val TIME_IN_FOREGROUND = 10L

val  now by lazy {
    Calendar.getInstance().timeInMillis
}

fun Job.status(): Int = when {
    isActive -> 1
    isCompleted && isCancelled -> 0
    isCancelled -> 0
    isCompleted -> 0
    else -> 2
}

fun hashPassword(password: String): String {
    val bcryptHashString = BCrypt.withDefaults().hash(12, "salt1234567Bytes".toByteArray(), password.toByteArray())
    return bcryptHashString.toString(Charsets.UTF_8)
}