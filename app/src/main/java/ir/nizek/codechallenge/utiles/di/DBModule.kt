package ir.nizek.codechallenge.utiles.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ir.nizek.codechallenge.database.AppDatabase
import ir.nizek.codechallenge.database.dao.UserDao
import ir.nizek.codechallenge.repository.UserRepository
import ir.nizek.codechallenge.utiles.dbName
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DBModule {
    @Singleton
    @Provides
    fun provideUserDao(@ApplicationContext appContext: Context): UserDao {
        val db = Room.databaseBuilder(appContext, AppDatabase::class.java, dbName).build()
        return db.getUserDao()
    }

    @Singleton
    @Provides
    fun provideUserRepository(dao: UserDao): UserRepository  {
        return UserRepository(dao)
    }
}