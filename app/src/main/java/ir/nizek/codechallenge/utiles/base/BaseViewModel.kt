package ir.nizek.codechallenge.utiles.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel: ViewModel() {
    protected var _errorLiveData: MutableLiveData<String> = MutableLiveData()

    val errorLiveData: LiveData<String>
    get() {
        return _errorLiveData
    }
}