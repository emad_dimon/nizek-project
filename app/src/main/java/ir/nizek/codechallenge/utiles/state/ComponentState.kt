package ir.nizek.codechallenge.utiles.state

import androidx.lifecycle.*

abstract class ComponentState(lifecycleOwner: LifecycleOwner) {
    enum class LifecycleState {
        START,
        STOP,
        Destroy
    }
    init {
        val lifecycle = lifecycleOwner.lifecycle
        if (lifecycle.currentState != Lifecycle.State.DESTROYED) {
            lifecycle.addObserver(LifecycleObserver {
                state ->
                when(state) {
                    LifecycleState.START -> componentInForeground()
                    LifecycleState.STOP -> componentInBackground()
                    LifecycleState.Destroy -> release()
                }
            })
        }
    }

    abstract fun componentInForeground()
    abstract fun componentInBackground()
    abstract fun release()

    class LifecycleObserver(private val callback: (LifecycleState) -> Unit): DefaultLifecycleObserver {

        override fun onStart(owner: LifecycleOwner) {
            super.onStart(owner)
            callback.invoke(LifecycleState.START)
        }

        override fun onStop(owner: LifecycleOwner) {
            super.onStop(owner)
            callback.invoke(LifecycleState.STOP)
        }

        override fun onDestroy(owner: LifecycleOwner) {
            super.onDestroy(owner)
            callback.invoke(LifecycleState.Destroy)
        }
    }
}
