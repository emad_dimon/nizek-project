package ir.nizek.codechallenge.utiles.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

@InstallIn(ViewModelComponent::class)
@Module
object ViewModelModule {

    @ViewModelScoped
    @Provides
    fun provideMainDispatcher(): CoroutineContext {
        return Dispatchers.Main
    }
}