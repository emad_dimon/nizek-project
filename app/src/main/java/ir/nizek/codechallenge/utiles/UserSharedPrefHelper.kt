package ir.nizek.codechallenge.utiles

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

class UserSharedPrefHelper {
    private var pref: SharedPreferences? = null
    private var _isLoggedIn: Boolean = false
    private var _isRegistered: Boolean = false

    var isLoggedIn: Boolean
    get() = _isLoggedIn
    set(value) {
        val prefEditor = pref?.edit()
        prefEditor?.putBoolean("loggedIn", value)
        prefEditor?.apply()

        _isLoggedIn = value
        _isRegistered = true
    }

    val isRegistered: Boolean
    get() {
        return _isRegistered
    }

    companion object {
        var instanse: UserSharedPrefHelper? = null
        fun getInstance(context: Context): UserSharedPrefHelper {
            return instanse ?: synchronized(this) {
                instanse ?: UserSharedPrefHelper().also {
                    it.isUserLoggedIn(context)
                    instanse = it
                }
            }
        }
    }

    private fun isUserLoggedIn(context: Context) {
        pref = context.getSharedPreferences(userSharedPrefKey, MODE_PRIVATE)
        _isRegistered = pref?.contains("loggedIn") ?: false
        _isLoggedIn = pref?.getBoolean("loggedIn", false) ?: false
    }
}