package ir.nizek.codechallenge.domain.user

import ir.nizek.codechallenge.database.entity.UserEntity
import ir.nizek.codechallenge.domain.EntityMapper

class UserMapper: EntityMapper<UserEntity, User> {
    override fun entityToDomainModel(entity: UserEntity): User {
        return User(
            entity.id,
            entity.name,
            entity.username,
            entity.password,
            entity.createdAt,
            entity.updatedAt
        )
    }

    override fun domainModelToEntity(model: User): UserEntity {
        return UserEntity(
            model.name,
            model.username,
            model.password,
            model.createdAt,
            model.updatedAt
        )
    }

    fun entitiesToListOfDomain(list: List<UserEntity>): List<User> {
        return list.map {
            entityToDomainModel(it)
        }
    }
}