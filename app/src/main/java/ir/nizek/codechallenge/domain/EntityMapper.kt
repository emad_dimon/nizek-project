package ir.nizek.codechallenge.domain

interface EntityMapper<Entity, DomainModel> {
    fun entityToDomainModel(entity: Entity): DomainModel
    fun domainModelToEntity(model: DomainModel): Entity
}