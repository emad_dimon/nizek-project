package ir.nizek.codechallenge.domain.user

data class User(
    val id: Long? = null,
    val name: String? = null,
    val username: String,
    val password: String,
    val createdAt: Long? = null,
    val updatedAt: Long? = null,
)