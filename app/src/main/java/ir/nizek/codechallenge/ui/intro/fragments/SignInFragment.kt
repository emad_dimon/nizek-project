package ir.nizek.codechallenge.ui.intro.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ir.nizek.codechallenge.R
import ir.nizek.codechallenge.domain.user.User
import ir.nizek.codechallenge.ui.intro.IntroViewModel
import ir.nizek.codechallenge.utiles.*
import ir.nizek.codechallenge.utiles.base.BaseFragment
import ir.nizek.codechallenge.utiles.state.LimitationOnUsage
import ir.nizek.nbutton.NizekButton

@AndroidEntryPoint
class SignInFragment : BaseFragment() {

    private var securePassword: String? = null
    private var insertUserState: Boolean = false
    private var signInUserState: Boolean = false

    private val viewModel: IntroViewModel by hiltNavGraphViewModels(R.id.nav_graph)
    private val userData: UserSharedPrefHelper by lazy {
        UserSharedPrefHelper.getInstance(requireContext())
    }

    private fun navigateToDashboard() {
        findNavController().navigate(R.id.action_signin_fragment_to_intro_fragment)
    }

    private fun signUp(name: String, userName: String, password: String) {
        val user = User(
            name = name,
            username = userName,
            password = hashPassword(password),
            createdAt = now,
            updatedAt = now
        )
        insertUserState = false
        viewModel.insertUser(user)
    }

    private fun logeIn(userName: String, password: String) {
        val user = User(username = userName, password = password)
        securePassword = hashPassword(password)
        signInUserState = false

        viewModel.getUser(user)
    }

    override fun onStart() {
        super.onStart()
        viewModel.isLoggedIn(userData.isLoggedIn)
    }

    override fun observer() {
        with(viewModel) {
            insertUserLiveData.observe(viewLifecycleOwner) { id ->
                if (id > 0 && !insertUserState)
                    viewModel.isLoggedIn(true)
            }
            getUserLiveData.observe(viewLifecycleOwner) { user ->
                    if (user.password == securePassword && !signInUserState) {
                        viewModel.isLoggedIn(true)
                    }
            }

            errorLiveData.observe(viewLifecycleOwner) {
                    message ->
                Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            }

            isLoggedIn.observe(viewLifecycleOwner) {
                if (it) {
                    signInUserState = true
                    insertUserState = true

                    navigateToDashboard()
                }
            }
        }
    }

    override fun initView(compose: ComposeView) {
        compose.setContent {
            var name by remember {
                mutableStateOf("")
            }
            var username by remember {
                mutableStateOf("")
            }
            var password by remember {
                mutableStateOf("")
            }

            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                if (!userData.isRegistered) {
                    OutlinedTextField(
                        value = name, onValueChange = { name = it },
                        label = {
                            Text(text = getString(R.string.name_placeholder))
                        },
                        shape = RoundedCornerShape(8.dp)
                    )
                    Spacer(modifier = Modifier.padding(8.dp))
                }
                OutlinedTextField(
                    value = username, onValueChange = { username = it },
                    label = {
                        Text(text = getString(R.string.username_placeholder))
                    },
                    shape = RoundedCornerShape(8.dp)
                )
                Spacer(modifier = Modifier.padding(8.dp))
                OutlinedTextField(
                    value = password, onValueChange = { password = it },
                    label = {
                        Text(text = getString(R.string.password_placeholder))
                    },
                    shape = RoundedCornerShape(8.dp),
                    visualTransformation = PasswordVisualTransformation(),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)
                )
                Spacer(modifier = Modifier.padding(8.dp))
                NizekButton(
                    onClick = {
                        if (userData.isRegistered) {
                            logeIn(username, password)
                        } else {
                            signUp(name, username, password)
                        }

                    },
                    title = if (userData.isRegistered) getString(R.string.login) else getString(R.string.signup),
                    "with unique username"
                )
            }
        }
    }
}