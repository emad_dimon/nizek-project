package ir.nizek.codechallenge.ui.intro

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.nizek.codechallenge.domain.user.User
import ir.nizek.codechallenge.repository.UserRepository
import ir.nizek.codechallenge.utiles.base.BaseViewModel
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class IntroViewModel
@Inject constructor(
    private val repository: UserRepository
) : BaseViewModel() {

    private val _isLoggedIn: MutableLiveData<Boolean> = MutableLiveData()
    private val _getUserLiveData: MutableLiveData<User> = MutableLiveData()
    private val _insertUserLiveData: MutableLiveData<Long> = MutableLiveData()

    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        _errorLiveData.value = exception.toString()
    }

    val isLoggedIn: LiveData<Boolean>
        get() {
            return _isLoggedIn
        }

    val getUserLiveData: LiveData<User>
        get() {
            return _getUserLiveData
        }

    val insertUserLiveData: LiveData<Long>
        get() {
            return _insertUserLiveData
        }

    fun isLoggedIn(value: Boolean) {
        _isLoggedIn.postValue(value)
    }

    fun getUser(user: User) {
        viewModelScope.launch(exceptionHandler) {
            val storedUser = repository.getUser(user.username)
            _getUserLiveData.postValue(storedUser)
        }
    }

    fun insertUser(user: User) {
        viewModelScope.launch(exceptionHandler) {
            val userId = repository.insert(user)
            _insertUserLiveData.postValue(userId)
        }
    }
}