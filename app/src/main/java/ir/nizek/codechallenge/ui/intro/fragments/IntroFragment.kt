package ir.nizek.codechallenge.ui.intro.fragments

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ir.nizek.codechallenge.R
import ir.nizek.codechallenge.domain.user.User
import ir.nizek.codechallenge.ui.intro.IntroViewModel
import ir.nizek.codechallenge.utiles.*
import ir.nizek.codechallenge.utiles.base.BaseFragment
import ir.nizek.codechallenge.utiles.state.LimitationOnUsage
import ir.nizek.nbutton.NizekButton

@AndroidEntryPoint
class IntroFragment : BaseFragment() {
    private val viewModel: IntroViewModel by hiltNavGraphViewModels(R.id.nav_graph)
    private var user: User? = null
    private var username = mutableStateOf("")

    private val userData: UserSharedPrefHelper by lazy {
        UserSharedPrefHelper.getInstance(requireContext())
    }

    private fun navigateToLoginPage() {
        findNavController().navigate(R.id.action_intro_fragment_to_signin_fragment)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        LimitationOnUsage.Builder(this)
            .foregroundLimitation(TIME_IN_FOREGROUND)
            .backgroundLimitation(TIME_IN_BACKGROUND)
            .setCallBack {
                viewModel.isLoggedIn(false)
            }
            .errorHandler { message ->
                Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            }
            .build()
    }

    override fun observer() {
        with(viewModel) {
            getUserLiveData.observe(viewLifecycleOwner) { loggedInUser ->
                user = loggedInUser
                user?.username?.let { name ->
                    username.value = name
                }
            }

            errorLiveData.observe(viewLifecycleOwner) {
                message ->
                Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            }

            isLoggedIn.observe(viewLifecycleOwner) {
                userData.isLoggedIn = it
                if (!it) {
                    navigateToLoginPage()
                }
            }
        }
    }

    override fun initView(compose: ComposeView) {
        compose.setContent {
            Column(modifier = Modifier.padding(16.dp)) {
                Text(text = getString(R.string.welcome_message).replace("$", username.value))
                Spacer(modifier = Modifier.padding(8.dp))
                NizekButton(
                    onClick = {
                        viewModel.isLoggedIn(false)
                    },
                    title = "Logout",
                    icon = R.drawable.ic_baseline_arrow
                )
            }
        }
    }
}
